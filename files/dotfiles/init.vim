" Plugins
"
" Plugin installation managed by vim-plug
call plug#begin('~/.vim/plugged')
"
" notes and wiki management
Plug 'vimwiki/vimwiki'
" colorscheme
Plug 'dracula/vim', {'name': 'dracula'}
" git integration
Plug 'tpope/vim-fugitive'
" markdown highlighting
Plug 'tpope/vim-markdown'
" ranger integration
Plug 'kevinhwang91/rnvimr'
" langauge package
Plug 'sheerun/vim-polyglot'
" go-plugin for using tooling
Plug 'fatih/vim-go'
" fuzzy finder integration, including ripgrep
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
"
Plug 'mhinz/vim-startify'
"
call plug#end()

" Basic settings
"
"
syntax on
"
set encoding=UTF-8
set exrc
set tabstop=4 softtabstop=4
set expandtab
set smartindent
set guicursor=
set relativenumber
set nohlsearch
set hidden
set noerrorbells
set nu
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set termguicolors
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
set cmdheight=2
set laststatus=2
"
" statusline set up
" left side
set statusline=
set statusline+=%#Title#
set statusline+=\ %M
set statusline+=\ %y
set statusline+=\ %r
set statusline+=\ %F
" right side
set statusline+=%=
set statusline+=\ %c:%l/%L
set statusline+=\ %p%%
set statusline+=\ [%n]

" Colorscheme
colorscheme dracula

" Key Mappings
"
" leader key
let mapleader=" "
"
" leader combinations
nnoremap <leader>w :VimwikiIndex<CR>
"
" split navigation
"
" moving between splits with ctrl + vim-moves (j,k,l,h)
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l
"
" resize the splits with alt + vim-moves
nnoremap <M-j>  :resize -2<CR>
nnoremap <M-k>  :resize +2<CR>
nnoremap <M-h>  :vertical resize -2<CR>
nnoremap <M-l>  :vertical resize +2<CR>
"
" Tabs
"
" Alt + t to create a new tab
nnoremap <silent> <A-t> :tabnew<CR>
nnoremap <silent> <A-2> :tabmove +<CR>
nnoremap <silent> <A-1> :tabmove -<CR>
"
" Tab or shift + Tab to move right and left between
" tabs.
nnoremap <Tab> gt
nnoremap <S-Tab> gT

" Functions
"
" whitespace trimming
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun
"
augroup PHIL
    autocmd!
    autocmd BufWritepre + :call TrimWhitespace()
augroup END

" File explorer settings
"
" disable netrw
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1
"
"use ranger instead of netrw
let g:loaded_clipboard_provider = 1
let g:ranger_replace_netrw = 1

" Plugin Configs
"
" fzf
source $HOME/.config/nvim/plug-config/fzf.vim
" rnvimr (ranger plugin)
source $HOME/.config/nvim/plug-config/rnvim.vim
" startify
source $HOME/.config/nvim/plug-config/start-screen.vim
