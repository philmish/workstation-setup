" Config file for startify

" directory to store sessions in
let g:startify_session_dir = '~/.config/nvim/session'

" organizing startify UI with headers and starify types
let g:startify_lists = [
        \ { 'type': 'files',    'header': ['    Recent Files']      },
        \ { 'type': 'dir',      'header': ['    Directory: '. getcwd()]},
        \ { 'type': 'sessions', 'header': ['    Sessions']          },
        \ { 'type': 'bookmarks', 'header': ['   Bookmarks']         },
        \]

" set bookmarks
let g:startify_bookmarks = [
        \ { 'i': '~/.config/nvim/init.vim' },
        \ { 'b': '~/.bashrc' },
        \ { 'a': '~/.bash_aliases' },
        \ '~/repos/gitlab.com/philmish',
        \]

" autmoatically restarts sessions if a session file is found in
" the directory vim is opened in.
let g:startify_session_autoload = 1

" take care of buffers
let g:startify_session_delete_buffers = 1

" enable unicode
let g:startify_fortune_use_unicode = 1

" update sessions autmoatically
let g:startify_session_persistence = 1

" get rid of empty buffers and quit
let g:starify_enable_special = 0


