#!/bin/bash

BASEURL="cht.sh/"

TOPIC=""

KEYWORDS=""


Help() {
    echo "Welcome to the cht.sh terminal utility !"
    echo "****************************************"
    echo
    echo "This tool uses cht.sh by chubin"
    echo "https://github.com/chubin/cheat.sh"
    echo
    echo "This script shortens the required typing to use cht.sh."
    echo "Alias this script (i.e to cht) to search the cheatsheets"
    echo "with just a few keystrokes."
    echo
    echo "Syntax: chtSht [-t|-k|-h]"
    echo
    echo "Options:"
    echo "          t -> Topic you want to look up (i.e go, python, bash ...)"
    echo "          k -> List of keywords as a comma seperated string (i.e 1,2,3,4,5)"
    echo "          h -> Help Option"
    echo
    echo "Example: chtSht -t python -k append,file"
    echo
    echo "****************************************"
    echo
    echo "Author: philmish"
    echo
    echo "****************************************"
}

while getopts ':t:k:h' flag; do
    case "${flag}" in
        t) TOPIC="${OPTARG}" ;;
        k) KEYWORDS="$(echo ${OPTARG} | tr , +) " ;;
        h) Help; exit 1 ;;
    esac
done

if [ "$TOPIC" = "" ] || [ "$KEYWORDS" = "" ]; then
    echo "Please provide a topic and keywords to look up"
    echo
    Help
    exit 2
else
    curl "$BASEURL$TOPIC/$KEYWORDS"
fi


